(function() {
  'use strict';
  
  angular.module('app', ['ui.router','angular.filter'])
  .config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/');
  $stateProvider.state('persons', {
    url: '/',
    templateUrl: '/views/person/index.html',
    controller: 'personController'
  }).state('create', {
    url: '/create',
    templateUrl: '/views/person/create.html',
    controller: 'personController'
  }).state('edit', {
    url: '/edit/:id',
    templateUrl: '/views/person/create.html',
    controller: 'personController'
  }).state('details', {
    url: '/details/:id',
    templateUrl: '/views/person/details.html',
    controller: 'personController'
  });
  }).constant('globalConfig', {
    apiAddress: 'http://localhost:8080/api'
  });
})();