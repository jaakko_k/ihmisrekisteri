(function() {
  'use strict';
 
  angular
  .module('app')
  .controller('personController', Controller);
 
  Controller.$inject = ['$scope', '$rootScope', 'personService', '$state', '$stateParams'];
 
  function Controller($scope, $rootScope, personService, $state, $stateParams) {
    $scope.persons = [];
    
    if ($state.current.name == 'persons') {
      $rootScope.Title = 'Person Listing';
      personService.getPersons().then(function(res) {
        $scope.persons = res.data;
      }).catch(function(err) {
        console.log(err);
      });
    } else if ($state.current.name == 'details') {
      $rootScope.Title = 'Person Details';
      var id = $stateParams.id;
      personService.getPerson(id).then(function(res) {
        $scope.person = res.data;
      }).catch(function(err) {
        console.log(err);
      });
   
      $scope.deletePerson = function(id) {
        if (confirm('Poistetaanko ihminen?')) {
          personService.deletePerson(id).then(function(res) {
            if (res.data == 'deleted') {
              $state.go('persons', {}, { reload: true });
            } 
          }).catch(function(err) {
            console.log(err);
          });
        }
      };
    } else if ($state.current.name == 'edit') {
      $rootScope.Title = 'Edit Person';
      var id = $stateParams.id;
      personService.getPerson(id).then(function(res) {
        $scope.person = res.data;
      }).catch(function(err) {
        console.log(err);
      });
   
      $scope.saveData = function(person) {
        if ($scope.personForm.$valid) {
          personService.updatePerson(person).then(function(res) {
            if (res.data == 'updated') {
              $state.go('persons');
            }
          }).catch(function(err) {
            console.log(err);
          });
        }
      };
    } else if ($state.current.name == 'create') {
      $rootScope.Title = 'Create Person';
      $scope.saveData = function(person) {
        $scope.IsSubmit = true;
        if ($scope.personForm.$valid) {
          personService.createPerson(person).then(function(res) {
            if (res.data == 'created') {
              $state.go('persons');
            }
          }).catch(function(err) {
            console.log(err);
          });
        }
      };
    }
  }
})();