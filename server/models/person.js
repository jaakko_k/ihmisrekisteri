var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    objectId = mongoose.Schema.ObjectId;
    
var personSchema = new Schema({
  _id: {type: objectId, auto: true},
  firstname: {type: String, required: true},
  lastname: {type: String, required: true},
  address: {type: String, required: true},
  postalcode: {type: String, required: true},
  city: {type: String, required: true},
  phone: {type: String, required: true},
  email: {type: String, required: true},
  socsec: {type: String, required: true},
  gender: {type: String, required: true},
  strength: {type: Number, required: false},
  agility: {type: Number, required: false},
  constit: {type: Number, required: false},
  intell: {type: Number, required: false},
  wisdom: {type: Number, required: false},
  charisma: {type: Number, required: false},
});

var person = mongoose.model('persons', personSchema);

module.exports = person;