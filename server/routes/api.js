var express = require('express'),
    router = express.Router();
    
router.use('/person', require('../controllers/person.api'));

module.exports = router;